-r requirements.txt

pytest==6.1.1
pytest-cov==2.10.1
pytest-asyncio==0.14.0
pytest-mock==3.3.1
pytest-subprocess==1.0.0
black==20.8b1
pyfakefs==4.1.0
testfixtures==6.15.0
flake8==3.8.4
mkdocs==1.1.2
mkdocs-material==6.1.0
mkdocstrings==0.13.6
asynctest==0.13.0
