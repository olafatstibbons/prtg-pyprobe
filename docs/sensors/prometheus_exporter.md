---
**WARNING**  
This sensor is experimental.

---
#### Description
Monitors a Prometheus exporter and pulls select metrics.

---
#### Configuration
**Timeout**: If the reply takes longer than this value the request is aborted and an error message is triggered.  
**Exporter Port**:  Enter the port the Prometheus Exporter is listening on.  
**Metrics**:  Enter a list of comma separated metrics to capture.  
**Metric Path**:  Enter the path to the metrics, e.g. /metrics


---
![Prometheus Exporter](../img/sensor_prometheus_exporter.png){: align=left }
