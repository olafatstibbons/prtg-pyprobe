---
#### Description
Monitors the availability of a port on a target system.

---
#### Configuration
**Timeout**: If the reply takes longer than this value the request is aborted and an error message is triggered.  
**Port**:  This is the port that you want to monitor if it's open or closed.  


---
![Port](../img/sensor_port.png){: align=left }
