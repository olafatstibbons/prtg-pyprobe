---
**WARNING**  
Although this sensor can be added to every device on the probe, it will **ONLY** monitor the probe itself.  

---
#### Description
Sensor used to monitor the disk space usage of the Python probe.

---
#### Configuration
**Full Display of all disk space values**: Choose whether you want to get all disk space data or only percentages.

---
![Disk Usage](../img/sensor_disk_usage.png){: align=left }
