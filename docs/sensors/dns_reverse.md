---
#### Description
DNS PTR Lookup.

---
#### Configuration
**Timeout**:  If the reply takes longer than this value the request is aborted and an error message is triggered.  
**Port**:  Enter the port on which the DNS service of the parent device is running.  
**IP Address**: Enter am IP Address to resolve.

---
![DNS Reverse](../img/sensor_dns_reverse.png){: align=left }
