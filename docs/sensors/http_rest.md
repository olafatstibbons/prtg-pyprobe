---
#### Description
Sensor used to monitor the disk space usage of the Python probe.

---
#### Configuration
**Timeout**:  If the reply takes longer than this value the request is aborted and an error message is triggered.  
**HTTP Target**:  Enter a valid HTTP target to monitor.  The format should be http(s)://xyz.com.  
**Request Type**:  Select the request type.
**HTTP Headers**:  Enter HTTP Headers as dictionary. e.g. {'my header':'my value'}.   
**POST Data**:  Enter POST data as dictionary. e.g. {'my header':'my value'}. Only used when Request Type is POST.  
**Acceptable HTTP Status Codes**:  Enter acceptable HTTP Status codes. Format is comma separated. Leave empty for no filter.  
**Selector**:  Enter a valid selector for a value in XPATH notation. Leave empty if you only want to measure the API response time.  
**Value Type**:  The type of the value, the selector returns. Only int and float are supported. 

---
![HTTP REST](../img/sensor_http_rest.png){: align=left }
