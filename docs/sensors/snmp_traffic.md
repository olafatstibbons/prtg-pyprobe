---
#### Description
Monitors Traffic on provided interface using SNMP.

---
#### Configuration
**Interface Index (ifindex)**:  Please enter the ifIndex of the interface to be monitored.  
**Community String**:  Enter the community string.
**SNMP Port**:  Enter the SNMP Port.
**SNMP Version**: Choose your SNMP Version.

---
![SNMP Traffic](../img/sensor_snmp_traffic.png){: align=left }
