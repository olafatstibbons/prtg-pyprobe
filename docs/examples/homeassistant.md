# Run the prtg-pyprobe as Home Assistant Add On
- Open up the Add-On Store under Supervisor on your Home Assistant instance and add a new repository.
![Home Assistant](../img/ha_add_repository.png){: align=left }
- After you added the repository, the Add-On Store should look like this.
![Home Assistant](../img/ha_add_on_store_pyprobe.png){: align=left }
- Go to the Info tab and click on Install (dpending on your hardware this might take some time).
![Home Assistant](../img/ha_add_on_overview.png){: align=left }
- Now head over to the Configuration tab.
![Home Assistant](../img/ha_add_on_configuration.png){: align=left }
- The explanation for the different values can be found [here](../index.md#explanation-values-config_dict). Just save the config when you are done.
- Head over to Info again and enable **Watchdog** and **Auto update**.
![Home Assistant](../img/ha_add_on_ready.png){: align=left }
- Now click **Start** and check the **Logs**, it should look something like this.
![Home Assistant](../img/ha_add_on_logs.png){: align=left }
- You now can approve the probe in your PRTG web interface.



