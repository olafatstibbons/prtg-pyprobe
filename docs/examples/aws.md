# Run the prtg-pyprobe as an EC2 instance
- Download the Stack Template from the [releases page](https://gitlab.com/paessler-labs/prtg-pyprobe/-/releases)
- Log in to your AWS Account and head over to **Cloudformation**
- There choose **Create Stack** -> **With new resources**
- In Step one, change the **Template Source** to **Upload a template file** and choose the file you downloaded, then proceed
![AWS](../img/aws_create_stack_step1.png){: align=left }
- The **Stack Name** and the first 4 parameters (**InstanceType**, **KeyPairName**, **SubnetId**, **VPCId**) are free to choose (although we recommend keeping the InstanceType)
![AWS](../img/aws_create_stack_step2_part1.png){: align=left }
- Fill the pyprobe related paramters, where prefilled paramters usually don't have to be changed. For reference see the [general documentation](../index.md#explanation-values-config_dict). If you do not provide a **probeGid** a new one is created and the pyprobe will announce itself as new probe. 
![AWS](../img/aws_create_stack_step2_part2.png){: align=left }
- In step 3 there is no necessary configuration for the pyprobe
![AWS](../img/aws_create_stack_step3.png){: align=left }
- In step 4 review your settings and hit **Create Stack**
- This will create a new EC2 Instancec called **PRTG Pyprobe** and a security group called **PRTGPyprobeSecurityGroup** with no permissions whatsoever.
- You now can approve the probe in your PRTG web interface.


