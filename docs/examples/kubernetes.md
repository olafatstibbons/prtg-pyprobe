# Run the prtg-pyprobe on Kubernetes
## Rancher
### manual (via web interface)
- Add a new namespace.
  ![Rancher add namespace](../img/k8s_rancher_ns.png){: align=left }
- Add a new workload with a image from [this projects registry](https://gitlab.com/paessler-labs/prtg-pyprobe/container_registry) and add an Ennvironment Variable **PROBE_CONFIG** with contents as described [here](../index.md#format-of-the-config_dict-object).
  ![Rancher add workload](../img/k8s_rancher_deploy_workload_manual.png){: align=left }
- Head over to your PRTG Instance and approve the probe. 
### automated (rancher cli + kubectl)
- Add a new namespace:  
    `rancher namespace add prtg-pyprobe-cli`
- Add a new workload ([Example](#prtg-pyprobeyaml)):  
    `rancher kubectl apply -f prtg-pyprobe.yaml`  

## K8s
### kubectl
- Add a new namespace ([Example](#prtg-pyprobe-namespaceyaml)):  
   `kubectl create -f prtg-pyprobe-namespace.yaml`
- Add a new deployment ([Example](#prtg-pyprobeyaml)):
    `kubectl apply -f prtg-pyprobe.yaml`

### Helm
There is a Helm chart available at the [Helm Hub](https://artifacthub.io/packages/helm/prtg-pyprobe/pyprobe). To install follow these steps.  
IMPORTANT: This is only a very basic Helm chart without any advanced configuration like service users or security context.  
- Type ```helm repo add prtg-pyprobe http://prtg-pyprobe.s3-website-eu-west-1.amazonaws.com/helm-charts/``` to get the repo
- Now you can install the chart with 
````bash
helm install  \
--set namespace="YOUR_NAMESPACE"  \
--set probeConfig.probeAccessKey="YOUR_ACCESS_KEY" \
--set probeConfig.probeAccessKeyHashed="YOUR_ACCESS_KEY_HASHED" \
--set probeConfig.probeGid="YOUR_GID" \
--set probeConfig.prtgServerIpDns="PRTG_SERVER_IP_DNS" \
my-pyprobe prtg-pyprobe/pyprobe 
````
- Alternatively check out the repo and adjust the Helm Chart to your needs
- Afterwards you can install the chart with
````bash
helm template REPO_ROOT/deployment/helm/pyprobe\
--set namespace="YOUR_NAMESPACE"  \
--set probeConfig.probeAccessKey="YOUR_ACCESS_KEY" \
--set probeConfig.probeAccessKeyHashed="YOUR_ACCESS_KEY_HASHED" \
--set probeConfig.probeGid="YOUR_GID" \
--set probeConfig.prtgServerIpDns="PRTG_SERVER_IP_DNS" | kubectl apply -f -
````
The output of the helm template command should look somewhat like this
````json
---
# Source: pyprobe/templates/deployment.yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: prtg-pyprobe
  labels:
    helm.sh/chart: pyprobe-0.1.0
    app.kubernetes.io/name: pyprobe
    app.kubernetes.io/instance: RELEASE-NAME
    app.kubernetes.io/version: "1.0.0"
    app.kubernetes.io/managed-by: Helm
spec:
  replicas: 1
  selector:
    matchLabels:
      app.kubernetes.io/name: pyprobe
      app.kubernetes.io/instance: RELEASE-NAME
  template:
    metadata:
      labels:
        app.kubernetes.io/name: pyprobe
        app.kubernetes.io/instance: RELEASE-NAME
    spec:
      securityContext:
        {}
      containers:
        - name: pyprobe
          securityContext:
            {}
          image: "registry.gitlab.com/paessler-labs/prtg-pyprobe/pyprobe:1.0.0"
          imagePullPolicy: Always
          resources:
            {}
          env:
            - name: PROBE_CONFIG
              value: "{ 'disable_ssl_verification': 'false', 'log_file_location': '', 'log_level': 'INFO', 'probe_access_key': 'YOUR_ACCESS_KEY', 'probe_access_key_hashed': 'YOUR_ACCESS_KEY_HASHED', 'probe_base_interval':'60', 'probe_gid':'YOUR_GID', 'probe_name':'prtg-pyprobe', 'probe_protocol_version': '1', 'prtg_server_ip_dns': 'PRTG_SERVER_IP_DNS', 'prtg_server_port': '443', 'probe_task_chunk_size': '20'}"
````
- For more config possibilities see the [values.yml](https://gitlab.com/paessler-labs/prtg-pyprobe/-/tree/master/deployment/helm/pyprobe/values.yaml) file in this repo.




## Example Files  
### prtg-pyprobe.yaml
When using this file with kubectl (without rancher), replace `workload.user.cattle.io/workloadselector` with `app`. 
```yaml  
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    workload.user.cattle.io/workloadselector: deployment-prtg-pyprobe-cli-prtg-pyprobe-cli
  name: prtg-pyprobe-cli
  namespace: prtg-pyprobe-cli
spec:
  progressDeadlineSeconds: 600
  replicas: 1
  revisionHistoryLimit: 10
  selector:
    matchLabels:
      workload.user.cattle.io/workloadselector: deployment-prtg-pyprobe-cli-prtg-pyprobe-cli
    strategy:
      rollingUpdate:
        maxSurge: 1
        maxUnavailable: 0
      type: RollingUpdate
  template:
    metadata:
      labels:
        workload.user.cattle.io/workloadselector: deployment-prtg-pyprobe-cli-prtg-pyprobe-cli
      spec:
        containers:
        - env:
          - name: PROBE_CONFIG
            value: '{"disable_ssl_verification": false, "log_file_location": "", "log_level": "INFO", "probe_access_key": "YOUR ACCESS KEY", "probe_access_key_hashed": "YOUR ACCESS KEY HASHED", "probe_base_interval": "60", "probe_gid": "YOUR GID", "probe_name": "pyprobe K8s Rancher", "probe_protocol_version": "1", "prtg_server_ip_dns": "YOUR PRTG SERVER", "prtg_server_port": "443"}'
          image: registry.gitlab.com/paessler-labs/prtg-pyprobe/pyprobe:0.5.0
          imagePullPolicy: Always
          name: prtg-pyprobe-cli
        dnsPolicy: ClusterFirst
        restartPolicy: Always
        schedulerName: default-scheduler
        securityContext: {}
        terminationGracePeriodSeconds: 30
```

### prtg-pyprobe-namespace.yaml
```yaml
apiVersion: v1
kind: Namespace
metadata:
  name: prtg-pyprobe-cli
``` 
