import asyncio
import platform

import psutil
import pytest

import prtg_pyprobe.sensors.sensor_probe_memory as module
from prtg_pyprobe.sensors.sensor import SensorPSUtilBase
from tests.sensors.test_sensor_probehealth import uname_res, vmem, swapmem


def task_data_probehealth():
    return {
        "sensorid": "1234",
        "host": "127.0.0.1",
        "kind": "mpprobememory",
    }


class TestProbeMemorySensorProperties:
    def test_sensor_probe_memory_base_class(self, probe_memory_sensor):
        assert type(probe_memory_sensor).__bases__[0] is SensorPSUtilBase

    def test_sensor_probe_memory_name(self, probe_memory_sensor):
        assert probe_memory_sensor.name == "Memory (Probe)"

    def test_sensor_probe_memory_kind(self, probe_memory_sensor):
        assert probe_memory_sensor.kind == "mpprobememory"

    def test_sensor_probe_memory_definition(self, probe_memory_sensor):
        assert probe_memory_sensor.definition == {
            "description": "Sensor used to monitor the memory usage of the system on "
            "which the python probe is running.",
            "groups": [{"caption": "Probe Memory", "fields": [], "name": "probe_memory"}],
            "help": "Sensor used to monitor the memory usage of the system on which the "
            "python probe is running.This is always the case even if added to a "
            "different device than the Probe Device.",
            "kind": "mpprobememory",
            "name": "Memory (Probe)",
            "tag": "mpprobememorysensor",
        }


@pytest.mark.asyncio
class TestProbeHealthSensorWork:
    async def test_sensor_probehealth_work_success_disk_basic(self, probe_memory_sensor, monkeypatch):
        monkeypatch.setattr(platform, "uname", uname_res)
        monkeypatch.setattr(psutil, "virtual_memory", vmem)
        monkeypatch.setattr(psutil, "swap_memory", swapmem)

        probehealth_queue = asyncio.Queue()
        await probe_memory_sensor.work(task_data=task_data_probehealth(), q=probehealth_queue)
        out = await probehealth_queue.get()
        assert out == {
            "channel": [
                {"kind": "BytesMemory", "mode": "integer", "name": "Memory Total", "value": 17179869184},
                {"kind": "BytesMemory", "mode": "integer", "name": "Memory Available", "value": 5667594240},
                {"kind": "BytesMemory", "mode": "integer", "name": "Swap Total", "value": 2147483648},
                {"kind": "BytesMemory", "mode": "integer", "name": "Swap Free", "value": 1107820544},
            ],
            "message": "Memory usage on Darwin, everything OK",
            "sensorid": "1234",
        }

    async def test_sensor_probe_memory_psutil_exception(
        self, probe_memory_sensor, monkeypatch, sensor_exception_message, mocker
    ):
        logging_mock = mocker.patch.object(module, "logging")
        mocker.patch.object(probe_memory_sensor, "get_memory_usage", side_effect=psutil.Error())

        probehealth_queue = asyncio.Queue()
        await probe_memory_sensor.work(task_data=task_data_probehealth(), q=probehealth_queue)
        out = await probehealth_queue.get()
        sensor_exception_message["message"] = "Probe memory check failed. See log for details"
        assert out == sensor_exception_message
        logging_mock.exception.assert_called_once_with("Probe memory information could not be determined")
