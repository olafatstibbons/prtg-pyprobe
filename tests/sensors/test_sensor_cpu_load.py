import asyncio

import psutil
import pytest

import prtg_pyprobe.sensors.sensor_cpuload as module
from prtg_pyprobe.sensors.sensor import SensorPSUtilBase


def task_data_cpu_load():
    return {
        "sensorid": "1234",
        "host": "127.0.0.1",
        "kind": "mpcpuload",
    }


def cpu_load():
    return 0.1, 0.2, 0.3


def raise_error():
    raise psutil.Error


class TestSensorCPULoadProperties:
    def test_sensor_cpu_load_base_class(self, cpu_load_sensor):
        assert type(cpu_load_sensor).__bases__[0] is SensorPSUtilBase

    def test_sensor_cpu_load_name(self, cpu_load_sensor):
        assert cpu_load_sensor.name == "CPU Load (Probe)"

    def test_sensor_cpu_load_kind(self, cpu_load_sensor):
        assert cpu_load_sensor.kind == "mpcpuload"

    def test_sensor_cpu_load_definition(self, cpu_load_sensor):
        assert cpu_load_sensor.definition == {
            "description": "Monitors CPU load avg on the system the pyprobe is running " "on.",
            "groups": [],
            "help": "Monitors CPU load avg on the system the pyprobe is running on.This "
            "is always the case even if added to a different device than the "
            "Probe Device.",
            "kind": "mpcpuload",
            "name": "CPU Load (Probe)",
            "tag": "mpprobehealthsensor",
        }


@pytest.mark.asyncio
class TestSensorCPULoadWork:
    async def test_sensor_cpu_load_work_success(self, monkeypatch, cpu_load_sensor):
        monkeypatch.setattr(psutil, "getloadavg", cpu_load)
        cpu_load_queue = asyncio.Queue()
        await cpu_load_sensor.work(task_data=task_data_cpu_load(), q=cpu_load_queue)
        out = await cpu_load_queue.get()
        assert out == {
            "channel": [
                {"customunit": "", "kind": "Custom", "mode": "float", "name": "Load average 1 min", "value": 0.1},
                {"customunit": "", "kind": "Custom", "mode": "float", "name": "Load average 5 min", "value": 0.2},
                {"customunit": "", "kind": "Custom", "mode": "float", "name": "Load average 10 min", "value": 0.3},
            ],
            "message": "OK",
            "sensorid": "1234",
        }

    async def test_sensor_cpu_load_error(self, monkeypatch, cpu_load_sensor, sensor_exception_message, mocker):
        logging_mock = mocker.patch.object(module, "logging")
        monkeypatch.setattr(psutil, "getloadavg", raise_error)
        cpu_load_queue = asyncio.Queue()
        await cpu_load_sensor.work(task_data=task_data_cpu_load(), q=cpu_load_queue)
        out = await cpu_load_queue.get()
        sensor_exception_message["message"] = "CPU Load check failed. See log for details"
        assert out == sensor_exception_message
        logging_mock.exception.assert_called_with("CPU Load avg could not be determined")
        logging_mock.exception.assert_called_once()
