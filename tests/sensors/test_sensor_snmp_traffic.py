import asyncio

import pytest
from pysnmp.hlapi.asyncio import SnmpEngine

from prtg_pyprobe.sensors import sensor_snmp_traffic as module
from prtg_pyprobe.sensors.sensor import SensorSNMPBase


class TestSNMPTrafficSensorProperties:
    def test_sensor_snmp_traffic_base_class(self, snmp_traffic_sensor):
        assert type(snmp_traffic_sensor).__bases__[0] is SensorSNMPBase

    def test_sensor_snmp_custom_name(self, snmp_traffic_sensor):
        assert snmp_traffic_sensor.name == "SNMP Traffic"

    def test_sensor_snmp_custom_kind(self, snmp_traffic_sensor):
        assert snmp_traffic_sensor.kind == "mpsnmptraffic"

    def test_sensor_snmp_custom_definition(self, snmp_traffic_sensor):
        assert snmp_traffic_sensor.definition == {
            "description": "Monitors Traffic on provided interface using SNMP",
            "groups": [
                {
                    "caption": "Sensor Specific",
                    "fields": [
                        {
                            "caption": "Interface Index (ifindex)",
                            "help": "Please enter the ifIndex of the interface to " "be monitored.",
                            "name": "ifindex",
                            "required": "1",
                            "type": "edit",
                        }
                    ],
                    "name": "sensor_specific",
                },
                {
                    "caption": "SNMP Specific",
                    "fields": [
                        {
                            "caption": "Community String",
                            "help": "Please enter the community string.",
                            "name": "community",
                            "required": "1",
                            "type": "edit",
                        },
                        {
                            "caption": "SNMP Port",
                            "help": "Please enter SNMP Port.",
                            "name": "port",
                            "required": "1",
                            "type": "integer",
                        },
                        {
                            "caption": "SNMP Version",
                            "default": "1",
                            "help": "Choose your SNMP Version",
                            "name": "snmp_version",
                            "options": {"0": "V1", "1": "V2c", "2": "V3"},
                            "required": "1",
                            "type": "radio",
                        },
                    ],
                    "name": "snmpspecific",
                },
            ],
            "help": "Monitors Traffic on provided interface using SNMP",
            "kind": "mpsnmptraffic",
            "name": "SNMP Traffic",
            "tag": "mpsnmptrafficsensor",
        }


@pytest.mark.asyncio
class TestSNMPTrafficSensorWork:
    @staticmethod
    async def snmp_get_success(*args, **kwargs):
        return [("1.3.6.1.1.2.3.4", 1234), ("1.3.6.1.1.2.3.5", 5678)]

    @staticmethod
    async def snmp_get_empty_value(*args, **kwargs):
        return [("1.3.6.1.1.2.3.4", "")]

    async def test_sensor_snmp_traffic_work_success(
        self, snmp_traffic_sensor, snmp_traffic_sensor_taskdata, monkeypatch
    ):
        monkeypatch.setattr(SensorSNMPBase, "get", self.snmp_get_success)
        snmp_engine = SnmpEngine()
        test_q = asyncio.Queue()
        await snmp_traffic_sensor.work(task_data=snmp_traffic_sensor_taskdata, q=test_q, snmp_engine=snmp_engine)
        ret = await test_q.get()
        assert ret["sensorid"] == "1234"
        assert ret["message"] == "Interface with ifindex 1. OK!"
        assert {"name": "Traffic In", "mode": "counter", "value": 1234, "unit": "BytesBandwidth"} in ret["channel"]
        assert {"name": "Traffic Out", "mode": "counter", "value": 5678, "unit": "BytesBandwidth"} in ret["channel"]
        assert {"name": "Traffic Total", "mode": "counter", "value": 6912, "unit": "BytesBandwidth"} in ret["channel"]

    async def test_sensor_snmp_traffic_work_empty_value(
        self, mocker, snmp_traffic_sensor, snmp_traffic_sensor_taskdata, monkeypatch, sensor_exception_message
    ):
        logging_mock = mocker.patch.object(module, "logging")
        monkeypatch.setattr(SensorSNMPBase, "get", self.snmp_get_empty_value)
        snmp_engine = SnmpEngine()
        test_q = asyncio.Queue()
        await snmp_traffic_sensor.work(task_data=snmp_traffic_sensor_taskdata, q=test_q, snmp_engine=snmp_engine)
        ret = await test_q.get()
        sensor_exception_message["message"] = "SNMP Query failed. See logs for details"
        assert ret == sensor_exception_message
        logging_mock.exception.assert_called_with("There has been an error in PySNMP.")
        logging_mock.exception.assert_called_once()
