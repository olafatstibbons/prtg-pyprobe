import asyncio
import platform

import psutil
import pytest

import prtg_pyprobe.sensors.sensor_probe_diskspace as module
from prtg_pyprobe.sensors.sensor import SensorPSUtilBase
from tests.sensors.test_sensor_probehealth import (
    uname_res,
    part_disk_usage,
    disk_part,
)


def task_data_probe_disk_space(diskfull="1"):
    return {
        "sensorid": "1234",
        "diskfull": diskfull,
        "host": "127.0.0.1",
        "kind": "mpprobediskusage",
    }


class TestProbeDiskSpaceSensorProperties:
    def test_sensor_probe_disk_space_base_class(self, probe_disk_space_sensor):
        assert type(probe_disk_space_sensor).__bases__[0] is SensorPSUtilBase

    def test_sensor_probe_disk_space_name(self, probe_disk_space_sensor):
        assert probe_disk_space_sensor.name == "Disk Usage (Probe)"

    def test_sensor_probe_disk_space_kind(self, probe_disk_space_sensor):
        assert probe_disk_space_sensor.kind == "mpprobediskusage"

    def test_sensor_probe_disk_space_definition(self, probe_disk_space_sensor):
        assert (
            probe_disk_space_sensor.definition
            == {
                "description": "Sensor used to monitor the disk space usage of the Python " "probe.",
                "groups": [
                    {
                        "caption": "Disk Space Settings",
                        "fields": [
                            {
                                "caption": "Full Display of all disk space values",
                                "default": "1",
                                "help": "Choose whether you want to get all disk " "space data or only percentages.",
                                "name": "diskfull",
                                "options": {"1": "Percentages", "2": "Full Information"},
                                "type": "radio",
                            }
                        ],
                        "name": "diskspace",
                    }
                ],
                "help": "This sensor monitors the disk space usage of the probe itself.This "
                "is always the case even if added to a different device than the "
                "Probe Device.",
                "kind": "mpprobediskusage",
                "name": "Disk Usage (Probe)",
                "tag": "mpprobediskusage",
            }
            != {
                "description": "Sensor used to monitor the disk space usage of the Python " "probe.",
                "groups": [
                    {
                        "caption": "Disk Space Settings",
                        "fields": [
                            {
                                "caption": "Full Display of all disk space values",
                                "default": "1",
                                "help": "Choose wether you want to get all disk space " "data or only percentages.",
                                "name": "diskfull",
                                "options": {"1": "Percentages", "2": "Full Information"},
                                "type": "radio",
                            }
                        ],
                        "name": "diskspace",
                    }
                ],
                "help": "This sensor monitors the disk space usage of the probe itself.This "
                "is always the case even if added to a different device than the "
                "Probe Device.",
                "kind": "mpprobediskusage",
                "name": "Disk Usage (Probe)",
                "tag": "mpprobediskusage",
            }
        )


@pytest.mark.asyncio
class TestProbeHealthSensorWork:
    async def test_sensor_probe_disk_space_work_success_disk_basic(
        self, probe_disk_space_sensor, monkeypatch, psutil_system_temperatures, mocker
    ):
        monkeypatch.setattr(platform, "uname", uname_res)
        monkeypatch.setattr(psutil, "disk_partitions", disk_part)
        monkeypatch.setattr(psutil, "disk_usage", part_disk_usage)

        probe_disk_space_queue = asyncio.Queue()
        await probe_disk_space_sensor.work(task_data=task_data_probe_disk_space(), q=probe_disk_space_queue)
        out = await probe_disk_space_queue.get()
        assert out == {
            "channel": [
                {"kind": "Percent", "mode": "float", "name": "Disk Space Percent /", "value": 14.4},
                {"kind": "Percent", "mode": "float", "name": "Disk Space Percent /System/Volumes/Data", "value": 14.4},
                {"kind": "Percent", "mode": "float", "name": "Disk Space Percent /private/var/vm", "value": 14.4},
            ],
            "message": "Disk usage on Darwin, everything OK",
            "sensorid": "1234",
        }

    async def test_sensor_probe_disk_space_work_success_disk_full(self, probe_disk_space_sensor, monkeypatch):
        monkeypatch.setattr(platform, "uname", uname_res)
        monkeypatch.setattr(psutil, "disk_partitions", disk_part)
        monkeypatch.setattr(psutil, "disk_usage", part_disk_usage)

        probe_disk_space_queue = asyncio.Queue()
        await probe_disk_space_sensor.work(task_data=task_data_probe_disk_space(diskfull="2"), q=probe_disk_space_queue)
        out = await probe_disk_space_queue.get()
        assert out == {
            "channel": [
                {"kind": "Percent", "mode": "float", "name": "Disk Space Percent /", "value": 14.4},
                {"kind": "BytesDisk", "mode": "integer", "name": "Disk Space Total /", "value": 250685575168},
                {"kind": "BytesDisk", "mode": "integer", "name": "Disk Space Used /", "value": 11003236352},
                {"kind": "BytesDisk", "mode": "integer", "name": "Disk Space Free /", "value": 65329041408},
                {"kind": "Percent", "mode": "float", "name": "Disk Space Percent /System/Volumes/Data", "value": 14.4},
                {
                    "kind": "BytesDisk",
                    "mode": "integer",
                    "name": "Disk Space Total /System/Volumes/Data",
                    "value": 250685575168,
                },
                {
                    "kind": "BytesDisk",
                    "mode": "integer",
                    "name": "Disk Space Used /System/Volumes/Data",
                    "value": 11003236352,
                },
                {
                    "kind": "BytesDisk",
                    "mode": "integer",
                    "name": "Disk Space Free /System/Volumes/Data",
                    "value": 65329041408,
                },
                {"kind": "Percent", "mode": "float", "name": "Disk Space Percent /private/var/vm", "value": 14.4},
                {
                    "kind": "BytesDisk",
                    "mode": "integer",
                    "name": "Disk Space Total /private/var/vm",
                    "value": 250685575168,
                },
                {
                    "kind": "BytesDisk",
                    "mode": "integer",
                    "name": "Disk Space Used /private/var/vm",
                    "value": 11003236352,
                },
                {
                    "kind": "BytesDisk",
                    "mode": "integer",
                    "name": "Disk Space Free /private/var/vm",
                    "value": 65329041408,
                },
            ],
            "message": "Disk usage on Darwin, everything OK",
            "sensorid": "1234",
        }

    async def test_sensor_probe_disk_space_psutil_exception(
        self, probe_disk_space_sensor, monkeypatch, sensor_exception_message, mocker
    ):
        logging_mock = mocker.patch.object(module, "logging")
        mocker.patch.object(probe_disk_space_sensor, "get_partition_usage", side_effect=psutil.Error())

        probe_disk_space_queue = asyncio.Queue()
        await probe_disk_space_sensor.work(task_data=task_data_probe_disk_space(), q=probe_disk_space_queue)
        out = await probe_disk_space_queue.get()
        sensor_exception_message["message"] = "Probe disk space check failed. See log for details"
        assert out == sensor_exception_message
        logging_mock.exception.assert_called_once_with("Probe disk space could not be determined")
