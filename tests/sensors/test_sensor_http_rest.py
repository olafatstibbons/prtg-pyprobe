import asyncio
import datetime

import httpx
import pytest
from httpx import AsyncClient as Aclient
from httpx._models import Response, Request

from prtg_pyprobe.sensors import sensor_http_rest as module
from prtg_pyprobe.sensors.sensor import SensorBase


async def http_rest_sensor_response_200(*args, **kwargs):
    resp = Response(status_code=200, json={"value_int": 1, "value_float": 2.0, "value_str": "test"})
    resp.elapsed = datetime.timedelta(microseconds=25)
    return resp


async def raise_value_error(*args, **kwargs):
    raise ValueError


async def raise_key_error(*args, **kwargs):
    raise KeyError


async def raise_type_error(*args, **kwargs):
    raise TypeError


async def raise_httpx_error(*args, **kwargs):
    req = Request(method="GET", url="https//:example.com")
    raise httpx.HTTPError(message="Something happened", request=req)


def task_data_http_rest(
    request_type="GET", acceptable_status_codes="", headers="", post_data="", value_type="float", dpathselector=""
):
    return {
        "sensorid": "1234",
        "timeout": 5,
        "target": "https://example.com",
        "request_type": request_type,
        "acceptable_status_codes": acceptable_status_codes,
        "headers": headers,
        "post_data": post_data,
        "value_type": value_type,
        "dpathselector": dpathselector,
    }


class TestHTTPRESTSensorProperties:
    def test_sensor_http_base_class(self, http_rest_sensor):
        assert type(http_rest_sensor).__bases__[0] is SensorBase

    def test_sensor_http_name(self, http_rest_sensor):
        assert http_rest_sensor.name == "HTTP REST"

    def test_sensor_http_kind(self, http_rest_sensor):
        assert http_rest_sensor.kind == "mphttprest"

    def test_sensor_http_definition(self, http_rest_sensor):
        assert (
            http_rest_sensor.definition
            == {
                "description": "Monitors a REST response",
                "groups": [
                    {
                        "caption": "Target Information",
                        "fields": [
                            {
                                "caption": "Timeout (in s)",
                                "default": 10,
                                "help": "If the reply takes longer than this value "
                                "the request is aborted and an error message "
                                "is triggered. Maximum value is 900 sec. (= "
                                "15.0 minutes)",
                                "maximum": 900,
                                "minimum": 10,
                                "name": "timeout",
                                "required": "1",
                                "type": "integer",
                            },
                            {
                                "caption": "REST Target",
                                "default": "https://",
                                "help": "Enter a valid REST target to monitor.  The "
                                "format should be http(s)://xyz.com",
                                "name": "target",
                                "required": "1",
                                "type": "edit",
                            },
                            {
                                "caption": "Request Type",
                                "default": "GET",
                                "name": "request_type",
                                "options": {"GET": "GET", "POST": "POST"},
                                "required": "1",
                                "type": "radio",
                            },
                            {
                                "caption": "HTTP Headers",
                                "default": "",
                                "help": "Enter HTTP Headers as dictionary. e.g. {'my " "header':'my value'}",
                                "name": "headers",
                                "type": "edit",
                            },
                            {
                                "caption": "POST Data",
                                "default": "",
                                "help": "Enter POST data as dictionary. e.g. {'my "
                                "header':'my value'}. Only used when Request "
                                "Type is POST",
                                "name": "post_data",
                                "type": "edit",
                            },
                            {
                                "caption": "Acceptable HTTP Status Codes",
                                "default": "200",
                                "help": "Enter acceptable HTTP Status codes. Format "
                                "is comma separated. Leave empty for no "
                                "filter.",
                                "name": "acceptable_status_codes",
                                "type": "edit",
                            },
                        ],
                        "name": "targetinfo",
                    },
                    {
                        "caption": "REST Information",
                        "fields": [
                            {
                                "caption": "Selector",
                                "default": "",
                                "help": "Enter a valid selector for a value in XPATH "
                                "notation. Leave empty if you only want to "
                                "measure the API response time.",
                                "name": "dpathselector",
                                "type": "edit",
                            },
                            {
                                "caption": "Value Type",
                                "default": "float",
                                "name": "value_type",
                                "options": {"float": "Float", "integer": "Integer"},
                                "required": "1",
                                "type": "radio",
                            },
                        ],
                        "name": "restinfo",
                    },
                ],
                "help": "Monitors the availability and REST response of an HTTP target.",
                "kind": "mphttprest",
                "name": "HTTP REST",
                "tag": "mphttprestsensor",
            }
            != {
                "description": "Monitors a REST response",
                "groups": [
                    {
                        "caption": "Target Information",
                        "fields": [
                            {
                                "caption": "Timeout (in s)",
                                "default": 10,
                                "help": "If the reply takes longer than this value "
                                "the request is aborted and an error message "
                                "is triggered. Maximum value is 900 sec. (= "
                                "15.0 minutes)",
                                "maximum": 900,
                                "minimum": 10,
                                "name": "timeout",
                                "required": "1",
                                "type": "integer",
                            },
                            {
                                "caption": "REST Target",
                                "default": "https://",
                                "help": "Enter a valid REST target to monitor.  The "
                                "format should be http(s)://xyz.com",
                                "name": "target",
                                "required": "1",
                                "type": "edit",
                            },
                            {
                                "caption": "Request Type",
                                "default": "GET",
                                "name": "request_type",
                                "options": {"GET": "GET", "POST": "POST"},
                                "required": "1",
                                "type": "radio",
                            },
                            {
                                "caption": "HTTP Headers",
                                "default": "",
                                "help": "Enter HTTP Headers as dictionary. e.g. {'my " "header':'my value'}",
                                "name": "headers",
                                "type": "edit",
                            },
                            {
                                "caption": "POST Data",
                                "default": "",
                                "help": "Enter POST data as dictionary. e.g. {'my "
                                "header':'my value'}. Only used when Request "
                                "Type is POST",
                                "name": "post_data",
                                "type": "edit",
                            },
                            {
                                "caption": "Acceptable HTTP Status Codes",
                                "default": "200",
                                "help": "Enter acceptable HTTP Status codes. Format "
                                "is comma separated. Leave empty for no "
                                "filter.",
                                "name": "acceptable_status_codes",
                                "type": "edit",
                            },
                        ],
                        "name": "targetinfo",
                    },
                    {
                        "caption": "REST Information",
                        "fields": [
                            {
                                "caption": "Selector",
                                "default": "",
                                "help": "Enter a valid selector for a value in XPATH "
                                "notation. Leave empty if you only want to "
                                "measure the API response time.",
                                "name": "dpathselector",
                                "type": "edit",
                            },
                            {
                                "caption": "Request Type",
                                "default": "float",
                                "name": "value_type",
                                "options": {"float": "Float", "integer": "Integer"},
                                "required": "1",
                                "type": "radio",
                            },
                        ],
                        "name": "restinfo",
                    },
                ],
                "help": "Monitors the availability and REST response of an HTTP target.",
                "kind": "mphttprest",
                "name": "HTTP REST",
                "tag": "mphttprestsensor",
            }
        )


@pytest.mark.asyncio
class TestHTTPRESTSensorWork:
    async def test_sensor_http_rest_work(self, http_rest_sensor, monkeypatch):
        monkeypatch.setattr(Aclient, "send", http_rest_sensor_response_200)
        http_rest_queue = asyncio.Queue()
        await http_rest_sensor.work(task_data=task_data_http_rest(), q=http_rest_queue)
        http_rest_out = await http_rest_queue.get()
        assert http_rest_out["message"] == "HTTP target returned 200, Request Type: GET"
        assert http_rest_out["sensorid"] == "1234"
        assert {"name": "Response Time", "mode": "float", "value": 0.025, "kind": "TimeResponse"} in http_rest_out[
            "channel"
        ]
        assert {
            "name": "HTTP Status Code",
            "mode": "integer",
            "value": 200,
            "kind": "Custom",
            "customunit": "",
        } in http_rest_out["channel"]

    async def test_sensor_http_rest_work_cast_int_ok(self, http_rest_sensor, monkeypatch):
        monkeypatch.setattr(Aclient, "send", http_rest_sensor_response_200)
        http_rest_queue = asyncio.Queue()
        await http_rest_sensor.work(
            task_data=task_data_http_rest(dpathselector="/value_int", value_type="integer"), q=http_rest_queue
        )
        http_rest_out = await http_rest_queue.get()
        assert http_rest_out["message"] == "HTTP target returned 200, Request Type: GET"
        assert http_rest_out["sensorid"] == "1234"
        assert {"name": "Response Time", "mode": "float", "value": 0.025, "kind": "TimeResponse"} in http_rest_out[
            "channel"
        ]
        assert {
            "name": "HTTP Status Code",
            "mode": "integer",
            "value": 200,
            "kind": "Custom",
            "customunit": "",
        } in http_rest_out["channel"]
        assert {
            "name": "REST return value",
            "mode": "integer",
            "value": 1.0,
            "kind": "Custom",
            "customunit": "",
        } in http_rest_out["channel"]

    async def test_sensor_http_rest_work_cast_float_ok(self, http_rest_sensor, monkeypatch):
        monkeypatch.setattr(Aclient, "send", http_rest_sensor_response_200)
        http_rest_queue = asyncio.Queue()
        await http_rest_sensor.work(task_data=task_data_http_rest(dpathselector="/value_float"), q=http_rest_queue)
        http_rest_out = await http_rest_queue.get()
        assert http_rest_out["message"] == "HTTP target returned 200, Request Type: GET"
        assert http_rest_out["sensorid"] == "1234"
        assert {"name": "Response Time", "mode": "float", "value": 0.025, "kind": "TimeResponse"} in http_rest_out[
            "channel"
        ]
        assert {
            "name": "HTTP Status Code",
            "mode": "integer",
            "value": 200,
            "kind": "Custom",
            "customunit": "",
        } in http_rest_out["channel"]
        assert {
            "name": "REST return value",
            "mode": "float",
            "value": 2.0,
            "kind": "Custom",
            "customunit": "",
        } in http_rest_out["channel"]

    async def test_sensor_http_rest_work_cast_fail(
        self, http_rest_sensor, monkeypatch, mocker, sensor_exception_message
    ):
        logging_mock = mocker.patch.object(module, "logging")
        monkeypatch.setattr(Aclient, "send", http_rest_sensor_response_200)
        http_rest_queue = asyncio.Queue()
        await http_rest_sensor.work(task_data=task_data_http_rest(dpathselector="/value_str"), q=http_rest_queue)
        http_rest_out = await http_rest_queue.get()
        sensor_exception_message["message"] = "Could not cast the value xpath returned to the correct format."
        assert http_rest_out == sensor_exception_message
        logging_mock.exception.assert_called_with("Casting error.")
        logging_mock.exception.assert_called_once()

    async def test_sensor_http_rest_status_code_filter_ok(self, http_rest_sensor, monkeypatch):
        monkeypatch.setattr(Aclient, "send", http_rest_sensor_response_200)
        http_rest_queue = asyncio.Queue()
        await http_rest_sensor.work(task_data=task_data_http_rest(acceptable_status_codes="200"), q=http_rest_queue)
        http_rest_out = await http_rest_queue.get()
        assert http_rest_out["message"] == "HTTP target returned 200, Request Type: GET"
        assert http_rest_out["sensorid"] == "1234"
        assert {"name": "Response Time", "mode": "float", "value": 0.025, "kind": "TimeResponse"} in http_rest_out[
            "channel"
        ]
        assert {
            "name": "HTTP Status Code",
            "mode": "integer",
            "value": 200,
            "kind": "Custom",
            "customunit": "",
        } in http_rest_out["channel"]

    async def test_sensor_http_rest_status_code_filter_fail(
        self, http_rest_sensor, monkeypatch, mocker, sensor_exception_message
    ):
        logging_mock = mocker.patch.object(module, "logging")
        monkeypatch.setattr(Aclient, "send", http_rest_sensor_response_200)
        http_rest_queue = asyncio.Queue()
        await http_rest_sensor.work(task_data=task_data_http_rest(acceptable_status_codes="300"), q=http_rest_queue)
        http_rest_out = await http_rest_queue.get()
        sensor_exception_message["message"] = "Status Code 200 not in acceptable list"
        assert http_rest_out == sensor_exception_message
        logging_mock.exception.assert_called_with("HTTP Status did not match any in the acceptable list.")
        logging_mock.exception.assert_called_once()

    async def test_sensor_http_rest_work_status_bad_headers_fail(
        self, http_rest_sensor, monkeypatch, mocker, sensor_exception_message
    ):
        logging_mock = mocker.patch.object(module, "logging")
        monkeypatch.setattr(Aclient, "send", http_rest_sensor_response_200)
        http_queue = asyncio.Queue()
        await http_rest_sensor.work(task_data=task_data_http_rest(headers="{bad=headers}"), q=http_queue)
        http_out = await http_queue.get()
        sensor_exception_message["message"] = "POST Data, HTTP Headers or response not in correct format."
        assert http_out == sensor_exception_message
        logging_mock.exception.assert_called_with("POST data, headers or response not in correct format")
        logging_mock.exception.assert_called_once()

    async def test_sensor_http_rest_work_status_bad_postdata_fail(
        self, http_rest_sensor, monkeypatch, mocker, sensor_exception_message
    ):
        logging_mock = mocker.patch.object(module, "logging")
        monkeypatch.setattr(Aclient, "send", http_rest_sensor_response_200)
        http_queue = asyncio.Queue()
        await http_rest_sensor.work(
            task_data=task_data_http_rest(request_type="POST", post_data="{bad=postdata}"),
            q=http_queue,
        )
        http_out = await http_queue.get()
        sensor_exception_message["message"] = "POST Data, HTTP Headers or response not in correct format."
        assert http_out == sensor_exception_message
        logging_mock.exception.assert_called_with("POST data, headers or response not in correct format")
        logging_mock.exception.assert_called_once()

    async def test_sensor_http_rest_work_status_success_postdata_headers_success(self, http_rest_sensor, monkeypatch):
        monkeypatch.setattr(Aclient, "send", http_rest_sensor_response_200)
        http_rest_queue = asyncio.Queue()
        await http_rest_sensor.work(
            task_data=task_data_http_rest(
                request_type="POST",
                post_data='{"post": "data"}',
                headers='{"good": "headers"}',
            ),
            q=http_rest_queue,
        )
        http_rest_out = await http_rest_queue.get()
        print(http_rest_out)
        assert http_rest_out["message"] == "HTTP target returned 200, Request Type: POST"
        assert http_rest_out["sensorid"] == "1234"

        assert {
            "name": "HTTP Status Code",
            "mode": "integer",
            "value": 200,
            "kind": "Custom",
            "customunit": "",
        } in http_rest_out["channel"]
        assert {"name": "Response Time", "mode": "float", "value": 0.025, "kind": "TimeResponse"} in http_rest_out[
            "channel"
        ]

    async def test_sensor_http_rest_work_type_error(
        self, http_rest_sensor, monkeypatch, mocker, sensor_exception_message
    ):
        logging_mock = mocker.patch.object(module, "logging")
        monkeypatch.setattr(Aclient, "send", raise_type_error)
        http_queue = asyncio.Queue()
        await http_rest_sensor.work(task_data=task_data_http_rest(), q=http_queue)
        http_out = await http_queue.get()
        sensor_exception_message["message"] = "Response not in correct format."
        assert http_out == sensor_exception_message
        logging_mock.exception.assert_called_with("Response not in correct format")
        logging_mock.exception.assert_called_once()

    async def test_sensor_http_rest_work_value_error(
        self, http_rest_sensor, monkeypatch, mocker, sensor_exception_message
    ):
        logging_mock = mocker.patch.object(module, "logging")
        monkeypatch.setattr(Aclient, "send", raise_value_error)
        http_queue = asyncio.Queue()
        await http_rest_sensor.work(task_data=task_data_http_rest(), q=http_queue)
        http_out = await http_queue.get()
        sensor_exception_message["message"] = "Query returned more than one element."
        assert http_out == sensor_exception_message
        logging_mock.exception.assert_called_with("Too much elements found in response. Xpath selector wrong?")
        logging_mock.exception.assert_called_once()

    async def test_sensor_http_rest_work_key_error(
        self, http_rest_sensor, monkeypatch, mocker, sensor_exception_message
    ):
        logging_mock = mocker.patch.object(module, "logging")
        monkeypatch.setattr(Aclient, "send", raise_key_error)
        http_queue = asyncio.Queue()
        await http_rest_sensor.work(task_data=task_data_http_rest(), q=http_queue)
        http_out = await http_queue.get()
        sensor_exception_message["message"] = "Element not found in response."
        assert http_out == sensor_exception_message
        logging_mock.exception.assert_called_with("Element not found in response. Xpath selector wrong?")
        logging_mock.exception.assert_called_once()

    async def test_sensor_http_rest_work_httpx_error(
        self, http_rest_sensor, monkeypatch, mocker, sensor_exception_message
    ):
        logging_mock = mocker.patch.object(module, "logging")
        monkeypatch.setattr(Aclient, "send", raise_httpx_error)
        http_queue = asyncio.Queue()
        await http_rest_sensor.work(task_data=task_data_http_rest(), q=http_queue)
        http_out = await http_queue.get()
        sensor_exception_message["message"] = "HTTP request failed. See log for details"
        assert http_out == sensor_exception_message
        logging_mock.exception.assert_called_with("There was an error in httpx.")
        logging_mock.exception.assert_called_once()
