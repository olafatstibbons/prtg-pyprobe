import asyncio

import pytest

from prtg_pyprobe.sensors import sensor_script as module
from prtg_pyprobe.sensors.sensor import SensorBase


def task_data_script(output_type="BASIC", script_name="test.sh"):
    return {
        "sensorid": "1234",
        "script_args": "",
        "host": "127.0.0.1",
        "timeout": "10",
        "kind": "mpscript",
        "script_name": script_name,
        "script_out_unit": "#",
        "output_type": output_type,
    }


async def create_shell_proc_work_basic(*args, **kwargs):
    m = MockProcWorkBASIC
    return m


async def create_shell_proc_work_json(*args, **kwargs):
    m = MockProcWorkJSON
    return m


async def create_shell_proc_fail(*args, **kwargs):
    m = MockProcFail
    return m


class MockProcWorkBASIC:
    @staticmethod
    async def communicate(*args, **kwargs):
        return b"hello:123", b""


class MockProcFail:
    @staticmethod
    async def communicate(*args, **kwargs):
        return b"", b"error"


class MockProcWorkJSON:
    @staticmethod
    async def communicate(*args, **kwargs):
        return b'{"name": "hello","value": 123}', b""


class TestScriptSensorProperties:
    def test_sensor_script_base_class(self, script_sensor):
        assert type(script_sensor).__bases__[0] is SensorBase

    def test_sensor_script_name(self, script_sensor):
        assert script_sensor.name == "Script (Probe)"

    def test_sensor_script_kind(self, script_sensor):
        assert script_sensor.kind == "mpscript"

    def test_sensor_script_definition(self, script_sensor):
        assert script_sensor.definition == {
            "description": "Execute shell scripts on the probe system",
            "groups": [
                {
                    "caption": "Script Settings",
                    "fields": [
                        {
                            "caption": "Timeout (in s)",
                            "default": 10,
                            "help": "If the reply takes longer than this value "
                            "the request is aborted and an error message "
                            "is triggered. Maximum value is 900 sec. (= "
                            "15.0 minutes)",
                            "maximum": 900,
                            "minimum": 10,
                            "name": "timeout",
                            "required": "1",
                            "type": "integer",
                        },
                        {
                            "caption": "Script Name",
                            "help": "Enter the name of the script located in "
                            "/var/prtg/scripts (you might have to mount "
                            "the directory when running the pyprobe "
                            "container)",
                            "name": "script_name",
                            "required": "1",
                            "type": "edit",
                        },
                        {
                            "caption": "Script Arguments",
                            "help": "Enter the arguments which should be passed " "to your script",
                            "name": "script_args",
                            "type": "edit",
                        },
                        {
                            "caption": "Script output unit",
                            "help": "Enter the unit which should be displayed for " "your script values.",
                            "name": "script_out_unit",
                            "type": "edit",
                        },
                        {
                            "caption": "Script output type",
                            "default": "BASIC",
                            "help": "Choose which output your script producse. "
                            "BASIC -> 'channelname:value' or JSON -> "
                            "check docs for format.",
                            "name": "output_type",
                            "options": {"BASIC": "BASIC", "JSON": "JSON"},
                            "required": "1",
                            "type": "radio",
                        },
                    ],
                    "name": "scriptsettings",
                }
            ],
            "help": "Execute shell scripts on the probe system",
            "kind": "mpscript",
            "name": "Script (Probe)",
            "tag": "mpscriptsensor",
        }


@pytest.mark.asyncio
class TestScriptSensorWork:
    script_out_work = {
        "sensorid": "1234",
        "message": "Running script test.sh. OK.",
        "channel": [
            {
                "name": "hello",
                "mode": "float",
                "value": 123.0,
                "kind": "Custom",
                "unit": "Custom",
                "customunit": "#",
            }
        ],
    }

    async def test_sensor_script_work_basic(self, script_sensor, monkeypatch):
        monkeypatch.setattr(asyncio, "create_subprocess_shell", create_shell_proc_work_basic)
        script_queue = asyncio.Queue()
        await script_sensor.work(task_data=task_data_script(), q=script_queue)
        script_out = await script_queue.get()
        assert script_out == self.script_out_work

    async def test_sensor_script_work_json(self, script_sensor, monkeypatch):
        monkeypatch.setattr(asyncio, "create_subprocess_shell", create_shell_proc_work_json)
        script_queue = asyncio.Queue()
        await script_sensor.work(task_data=task_data_script(output_type="JSON"), q=script_queue)
        script_out = await script_queue.get()
        assert script_out == self.script_out_work

    async def test_sensor_script_fail(self, script_sensor, monkeypatch, mocker, sensor_exception_message):
        logging_mock = mocker.patch.object(module, "logging")
        monkeypatch.setattr(asyncio, "create_subprocess_shell", create_shell_proc_fail)
        script_queue = asyncio.Queue()
        await script_sensor.work(task_data=task_data_script(), q=script_queue)
        script_out = await script_queue.get()
        sensor_exception_message["message"] = "Executing script produced an error on stderr"
        assert script_out == sensor_exception_message
        logging_mock.exception.assert_called_with("Script execution produced an error.")
        logging_mock.exception.assert_called_once()

    @pytest.mark.parametrize("script_name_param", ["../test.sh", "./test.sh", "~/test.sh"])
    async def test_sensor_script_fail_name(
        self, script_sensor, monkeypatch, mocker, sensor_exception_message, script_name_param
    ):
        logging_mock = mocker.patch.object(module, "logging")
        monkeypatch.setattr(asyncio, "create_subprocess_shell", create_shell_proc_fail)
        script_queue = asyncio.Queue()
        await script_sensor.work(task_data=task_data_script(script_name=script_name_param), q=script_queue)
        script_out = await script_queue.get()
        sensor_exception_message["message"] = (
            "The script name provided is in wrong format, " "please use the format script.sh"
        )
        assert script_out == sensor_exception_message
        logging_mock.exception.assert_called_with("Script name wrong.")
        logging_mock.exception.assert_called_once()
