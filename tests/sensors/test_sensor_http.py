import asyncio
import datetime

import httpx
import pytest
from httpx import AsyncClient as Aclient
from httpx._models import Response, Request

from prtg_pyprobe.sensors import sensor_http as module
from prtg_pyprobe.sensors.sensor import SensorBase


async def http_sensor_response_200(*args, **kwargs):
    resp = Response(
        status_code=200,
    )
    resp.elapsed = datetime.timedelta(microseconds=25)
    return resp


async def http_sensor_exception(*args, **kwargs):
    req = Request(method="GET", url="https//:example.com")
    raise httpx.ProtocolError(message="Protocol Error", request=req)


def task_data_http(request_type="GET", acceptable_status_codes="", headers="", post_data=""):
    return {
        "sensorid": "1234",
        "timeout": 5,
        "target": "https://example.com",
        "request_type": request_type,
        "acceptable_status_codes": acceptable_status_codes,
        "headers": headers,
        "post_data": post_data,
    }


class TestHTTPSensorProperties:
    def test_sensor_http_base_class(self, http_sensor):
        assert type(http_sensor).__bases__[0] is SensorBase

    def test_sensor_http_name(self, http_sensor):
        assert http_sensor.name == "HTTP"

    def test_sensor_http_kind(self, http_sensor):
        assert http_sensor.kind == "mphttp"

    def test_sensor_http_definition(self, http_sensor):
        assert http_sensor.definition == {
            "description": "Monitors an HTTP target",
            "groups": [
                {
                    "caption": "Target Information",
                    "fields": [
                        {
                            "caption": "Timeout (in s)",
                            "default": 10,
                            "help": "If the reply takes longer than this value "
                            "the request is aborted and an error message "
                            "is triggered. Maximum value is 900 sec. (= "
                            "15.0 minutes)",
                            "maximum": 900,
                            "minimum": 10,
                            "name": "timeout",
                            "required": "1",
                            "type": "integer",
                        },
                        {
                            "caption": "HTTP Target",
                            "default": "https://",
                            "help": "Enter a valid HTTP target to monitor.  The " "format should be http(s)://xyz.com",
                            "name": "target",
                            "required": "1",
                            "type": "edit",
                        },
                        {
                            "caption": "Request Type",
                            "default": "GET",
                            "name": "request_type",
                            "options": {"GET": "GET", "HEAD": "HEAD", "POST": "POST"},
                            "required": "1",
                            "type": "radio",
                        },
                        {
                            "caption": "HTTP Headers",
                            "default": "",
                            "help": "Enter HTTP Headers as dictionary. e.g. {'my " "header':'my value'}",
                            "name": "headers",
                            "type": "edit",
                        },
                        {
                            "caption": "POST Data",
                            "default": "",
                            "help": "Enter POST data as dictionary. e.g. {'my "
                            "header':'my value'}. Only used when Request "
                            "Type is POST",
                            "name": "post_data",
                            "type": "edit",
                        },
                        {
                            "caption": "Acceptable HTTP Status Codes",
                            "default": "200",
                            "help": "Enter acceptable HTTP Status codes. Format "
                            "is comma separated. Leave empty for no "
                            "filter.",
                            "name": "acceptable_status_codes",
                            "type": "edit",
                        },
                    ],
                    "name": "targetinfo",
                }
            ],
            "help": "Monitors the availability and response time of an HTTP target",
            "kind": "mphttp",
            "name": "HTTP",
            "tag": "mphttpsensor",
        }


@pytest.mark.asyncio
class TestHTTPSensorWork:
    async def test_sensor_http_work(self, http_sensor, monkeypatch):
        monkeypatch.setattr(Aclient, "send", http_sensor_response_200)
        http_queue = asyncio.Queue()
        await http_sensor.work(task_data=task_data_http(), q=http_queue)
        http_out = await http_queue.get()
        assert http_out["message"] == "HTTP target returned 200, Request Type: GET"
        assert http_out["sensorid"] == "1234"
        assert {
            "name": "HTTP Status Code",
            "mode": "integer",
            "value": 200,
            "kind": "Custom",
            "customunit": "",
        } in http_out["channel"]
        assert {
            "name": "Response Size",
            "mode": "float",
            "value": 49.0,
            "kind": "BytesFile",
        } in http_out["channel"]
        assert {"kind": "TimeResponse", "mode": "float", "name": "Response Time", "value": 0.025} in http_out["channel"]

    async def test_sensor_http_work_status_code_filter_ok(self, http_sensor, monkeypatch):
        monkeypatch.setattr(Aclient, "send", http_sensor_response_200)
        http_queue = asyncio.Queue()
        await http_sensor.work(task_data=task_data_http(acceptable_status_codes="200"), q=http_queue)
        http_out = await http_queue.get()
        assert http_out["message"] == "HTTP target returned 200, Request Type: GET"
        assert http_out["sensorid"] == "1234"
        assert {
            "name": "HTTP Status Code",
            "mode": "integer",
            "value": 200,
            "kind": "Custom",
            "customunit": "",
        } in http_out["channel"]
        assert {
            "name": "Response Size",
            "mode": "float",
            "value": 49.0,
            "kind": "BytesFile",
        } in http_out["channel"]
        assert {"kind": "TimeResponse", "mode": "float", "name": "Response Time", "value": 0.025} in http_out["channel"]

    async def test_sensor_http_work_status_code_filter_fail(
        self, http_sensor, monkeypatch, mocker, sensor_exception_message
    ):
        logging_mock = mocker.patch.object(module, "logging")
        monkeypatch.setattr(Aclient, "send", http_sensor_response_200)
        http_queue = asyncio.Queue()
        await http_sensor.work(task_data=task_data_http(acceptable_status_codes="300"), q=http_queue)
        http_out = await http_queue.get()
        sensor_exception_message["message"] = "Status Code 200 not in acceptable list"
        assert http_out == sensor_exception_message
        logging_mock.exception.assert_called_with("HTTP Status did not match any in the acceptable list.")
        logging_mock.exception.assert_called_once()

    async def test_sensor_http_work_status_bad_headers_fail(
        self, http_sensor, monkeypatch, mocker, sensor_exception_message
    ):
        logging_mock = mocker.patch.object(module, "logging")
        monkeypatch.setattr(Aclient, "send", http_sensor_response_200)
        http_queue = asyncio.Queue()
        await http_sensor.work(task_data=task_data_http(headers="{bad=headers}"), q=http_queue)
        http_out = await http_queue.get()
        sensor_exception_message["message"] = "POST Data or HTTP Headers not in correct format."
        assert http_out == sensor_exception_message
        logging_mock.exception.assert_called_with("POST data or headers not in correct format")
        logging_mock.exception.assert_called_once()

    async def test_sensor_http_work_http_error(self, http_sensor, monkeypatch, mocker, sensor_exception_message):
        logging_mock = mocker.patch.object(module, "logging")
        monkeypatch.setattr(Aclient, "send", http_sensor_exception)
        http_queue = asyncio.Queue()
        await http_sensor.work(task_data=task_data_http(), q=http_queue)
        http_out = await http_queue.get()
        sensor_exception_message["message"] = "HTTP request failed. See log for details"
        assert http_out == sensor_exception_message
        logging_mock.exception.assert_called_with("There was an error in httpx.")
        logging_mock.exception.assert_called_once()

    async def test_sensor_http_work_status_bad_postdata_fail(
        self, http_sensor, monkeypatch, mocker, sensor_exception_message
    ):
        logging_mock = mocker.patch.object(module, "logging")
        monkeypatch.setattr(Aclient, "send", http_sensor_response_200)
        http_queue = asyncio.Queue()
        await http_sensor.work(
            task_data=task_data_http(request_type="POST", post_data="{bad=postdata}"),
            q=http_queue,
        )
        http_out = await http_queue.get()
        sensor_exception_message["message"] = "POST Data or HTTP Headers not in correct format."
        assert http_out == sensor_exception_message
        logging_mock.exception.assert_called_with("POST data or headers not in correct format")
        logging_mock.exception.assert_called_once()

    async def test_sensor_http_work_status_success_postdata_headers_success(self, http_sensor, monkeypatch):
        monkeypatch.setattr(Aclient, "send", http_sensor_response_200)
        http_queue = asyncio.Queue()
        await http_sensor.work(
            task_data=task_data_http(
                request_type="POST",
                post_data='{"post": "data"}',
                headers='{"good": "headers"}',
            ),
            q=http_queue,
        )
        http_out = await http_queue.get()
        assert http_out["message"] == "HTTP target returned 200, Request Type: POST"
        assert http_out["sensorid"] == "1234"
        assert {
            "name": "HTTP Status Code",
            "mode": "integer",
            "value": 200,
            "kind": "Custom",
            "customunit": "",
        } in http_out["channel"]
        assert {
            "name": "Response Size",
            "mode": "float",
            "value": 49.0,
            "kind": "BytesFile",
        } in http_out["channel"]
        assert {"kind": "TimeResponse", "mode": "float", "name": "Response Time", "value": 0.025} in http_out["channel"]
