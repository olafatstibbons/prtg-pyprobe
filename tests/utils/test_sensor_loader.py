import pathlib

from prtg_pyprobe.sensors.sensor import SensorBase


class TestSensorLoader:
    def test_load_sensor_modules_is_subclass(self, list_sensor_modules):
        print(pathlib.Path().absolute())
        assert list_sensor_modules != {}
        for name, sensor in list_sensor_modules.items():
            assert issubclass(sensor.Sensor, SensorBase)

    def test_load_sensor_modules_has_attr(self, list_sensor_modules):
        print(pathlib.Path().absolute())
        assert list_sensor_modules != {}
        for name, sensor in list_sensor_modules.items():
            assert hasattr(sensor.Sensor, "name")
            assert hasattr(sensor.Sensor, "kind")
            assert hasattr(sensor.Sensor, "definition")

    def test_load_sensor_modules_naming_convention(self, list_sensor_modules):
        print(pathlib.Path().absolute())
        assert list_sensor_modules != {}
        for name, sensor in list_sensor_modules.items():
            assert name.startswith("sensor_")

    def test_create_sensor_objects_objects(self, list_sensor_objects):
        print(pathlib.Path().absolute())
        assert list_sensor_objects != []
        for sensor_object in list_sensor_objects:
            assert isinstance(sensor_object, SensorBase)
