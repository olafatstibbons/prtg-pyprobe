import json
import os

import pyfakefs
import pytest
from prompt_toolkit.document import Document
from prompt_toolkit.validation import ValidationError

from prtg_pyprobe.utils.config import configuration_questions
from prtg_pyprobe.utils.validators import ValidationFailedError

# Possible validation exception messages
ip_dns_validator_exception_message = "Please Enter a valid IP/DNS name."
port_range_validator_exception_message = "Please enter a valid port in the range 1 - 65535."
not_empty_validator_exception_message = "Input cannot be empty."
guid_validator_exception_message = "Probe GID has to be in GUID Format"
base_interval_validator_exception_message = "Interval has to be between 60 and 300."
base_chunk_size_validator_exception_message = "Chunk Size has to be between 5 and 50."
directory_validator_folder_exception_message = "Please specify a directory and filename that is writeable i.e."
directory_validator_permission_exception_message = (
    "Please specify a path that is writeable for the user running the probe"
)
directory_validator_log_stdout_message = (
    "Empty string specified, no logfile will be saved and logs will be printed to stdout!"
)
probe_cfg_path = "config.yml"


class TestConfigValidate:
    def test_validate_ip_correct(self, ip_dns_validator):
        doc = Document(text="12.34.56.78")
        assert ip_dns_validator.validate(doc) is None

    def test_validate_dns_correct(self, ip_dns_validator):
        doc = Document(text="test.com")
        assert ip_dns_validator.validate(doc) is None

    def test_validate_ip_incorrect(self, ip_dns_validator):
        doc = Document(text="1234.56.78.90")
        with pytest.raises(ValidationError) as e:
            ip_dns_validator.validate(doc)
        assert e.value.args[0] == ip_dns_validator_exception_message

    def test_validate_dns_incorrect(self, ip_dns_validator):
        doc = Document(text="thisisnotavaliddnsname")
        with pytest.raises(ValidationError) as e:
            ip_dns_validator.validate(doc)
        assert e.value.args[0] == ip_dns_validator_exception_message

    def test_validate_ip_dns_empty(self, ip_dns_validator):
        doc = Document(text="")
        with pytest.raises(ValidationError) as e:
            ip_dns_validator.validate(doc)
        assert e.value.args[0] == ip_dns_validator_exception_message

    def test_validate_port_correct(self, port_range_validator):
        doc = Document(text="25")
        assert port_range_validator.validate(doc) is None

    def test_validate_port_incorrect_high(self, port_range_validator):
        doc = Document(text="70000")
        with pytest.raises(ValidationError) as e:
            port_range_validator.validate(doc)
        assert e.value.args[0] == port_range_validator_exception_message

    def test_validate_port_incorrect_low(self, port_range_validator):
        doc = Document(text="0")
        with pytest.raises(ValidationError) as e:
            port_range_validator.validate(doc)
        assert e.value.args[0] == port_range_validator_exception_message

    def test_validate_port_incorrect_format(self, port_range_validator):
        doc = Document(text="hallo")
        with pytest.raises(ValidationError) as e:
            port_range_validator.validate(doc)
        assert e.value.args[0] == port_range_validator_exception_message

    def test_validate_port_empty(self, port_range_validator):
        doc = Document(text="")
        with pytest.raises(ValidationError) as e:
            port_range_validator.validate(doc)
        assert e.value.args[0] == port_range_validator_exception_message

    def test_validate_guid_correct(self, guid_validator):
        doc = Document(text="60A7C77C-0ED4-45C7-B40D-0A3FA0336EA6")
        assert guid_validator.validate(doc) is None

    def test_validate_guid_incorrect(self, guid_validator):
        doc = Document(text="1234gid")
        with pytest.raises(ValidationError) as e:
            guid_validator.validate(doc)
        assert e.value.args[0] == guid_validator_exception_message

    def test_validate_guid_empty(self, guid_validator):
        doc = Document(text="")
        with pytest.raises(ValidationError) as e:
            guid_validator.validate(doc)
        assert e.value.args[0] == guid_validator_exception_message

    def test_validate_interval_correct(self, base_interval_validator):
        doc = Document(text="60")
        assert base_interval_validator.validate(doc) is None

    def test_validate_interval_incorrect_low(self, base_interval_validator):
        doc = Document(text="55")
        with pytest.raises(ValidationError) as e:
            base_interval_validator.validate(doc)
        assert e.value.args[0] == base_interval_validator_exception_message

    def test_validate_interval_incorrect_high(self, base_interval_validator):
        doc = Document(text="301")
        with pytest.raises(ValidationError) as e:
            base_interval_validator.validate(doc)
        assert e.value.args[0] == base_interval_validator_exception_message

    def test_validate_interval_empty(self, base_interval_validator):
        doc = Document(text="")
        with pytest.raises(ValidationError) as e:
            base_interval_validator.validate(doc)
        assert e.value.args[0] == base_interval_validator_exception_message

    def test_validate_chunk_size_correct(self, base_chunk_size_validator):
        doc = Document(text="20")
        assert base_chunk_size_validator.validate(doc) is None

    def test_validate_chunk_size_incorrect_low(self, base_chunk_size_validator):
        doc = Document(text="2")
        with pytest.raises(ValidationError) as e:
            base_chunk_size_validator.validate(doc)
        assert e.value.args[0] == base_chunk_size_validator_exception_message

    def test_validate_chunk_size_incorrect_high(self, base_chunk_size_validator):
        doc = Document(text="60")
        with pytest.raises(ValidationError) as e:
            base_chunk_size_validator.validate(doc)
        assert e.value.args[0] == base_chunk_size_validator_exception_message

    def test_validate_chunk_size_empty(self, base_chunk_size_validator):
        doc = Document(text="")
        with pytest.raises(ValidationError) as e:
            base_chunk_size_validator.validate(doc)
        assert e.value.args[0] == base_chunk_size_validator_exception_message

    def test_not_empty_validator(self, not_empty_validator):
        doc = Document(text="")
        with pytest.raises(ValidationError) as e:
            not_empty_validator.validate(doc)
        assert e.value.args[0] == not_empty_validator_exception_message


class TestReadWriteConfig:
    def test_write_config(self, probe_config, probe_config_dict):
        probe_config.write(probe_config_dict)
        with open(probe_cfg_path, "r") as f:
            test_write_cfg = f.read()
            f.close()
        assert os.path.exists(probe_cfg_path)
        os.remove(probe_cfg_path)
        for k, v in probe_config_dict.items():
            assert k in test_write_cfg
            assert v is not None

    def test_read_config_file(self, probe_config, probe_config_dict):
        probe_config.write(probe_config_dict)
        test_read_cfg = probe_config.read()
        os.remove(probe_cfg_path)
        assert test_read_cfg == probe_config_dict

    def test_read_config_env(self, probe_config, probe_config_dict):
        os.environ["PROBE_CONFIG"] = json.dumps(probe_config_dict)
        test_read_cfg = probe_config.read()
        assert test_read_cfg == probe_config_dict
        os.environ.pop("PROBE_CONFIG")

    def test_read_config_env_validation_error(self, probe_config):
        os.environ["PROBE_CONFIG"] = json.dumps({"bad": "config"})
        with pytest.raises(ValidationFailedError):
            assert probe_config.read()
        os.environ.pop("PROBE_CONFIG")

    def test_read_config_missing(self, probe_config):
        with pytest.raises(KeyError):
            assert probe_config.read()

    def test_config_questions(
        self,
    ):
        assert type(configuration_questions) is list

    def test_directory_writeable_validator_success(self, fs, directory_writeable_validator):
        fs.create_dir("/test/working/")
        doc = Document(text="/test/working/config.yml")
        directory_writeable_validator.validate(doc)
        with open("/test/working/config.yml", "r") as f:
            assert f.read() == "This is your logfile"

    def test_directory_writeable_validator_folder_error(self, fs, directory_writeable_validator):
        fs.create_dir("/test/folder/")
        doc = Document(text="/test/folder/")
        with pytest.raises(ValidationError) as e:
            directory_writeable_validator.validate(doc)
        assert directory_validator_folder_exception_message in e.value.args[0]

    def test_directory_writeable_validator_permission_error(self, fs, directory_writeable_validator):
        fs.create_dir("/test/permssion/", perm_bits=000)
        doc = Document(text="/test/permssion/")
        pyfakefs.fake_filesystem.set_uid(1)
        with pytest.raises(ValidationError) as e:
            directory_writeable_validator.validate(doc)
        assert directory_validator_permission_exception_message in e.value.args[0]

    def test_directory_writeable_validator_no_log(self, directory_writeable_validator, capsys):
        doc = Document(text="")
        directory_writeable_validator.validate(doc)
        captured = capsys.readouterr()
        assert directory_validator_log_stdout_message in captured.out
