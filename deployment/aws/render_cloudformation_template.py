import json
import os

import boto3

from troposphere import Template, Base64, Ref, Parameter, GetAtt, Join, FindInMap
import troposphere.ec2 as ec2
import troposphere.policies as policies

RefRegion = Ref("AWS::Region")
RefStackId = Ref("AWS::StackId")
RefStackName = Ref("AWS::StackName")


class CloudformationTemplate(Template):
    def __init__(self):
        super().__init__()
        self.regions = [
            "us-east-1",
            "us-east-2",
            "us-west-1",
            "us-west-2",
            "ca-central-1",
            "eu-north-1",
            "eu-west-1",
            "eu-west-2",
            "eu-west-3",
            "eu-central-1",
            "ap-northeast-1",
            "ap-northeast-2",
            "ap-southeast-1",
            "ap-southeast-2",
            "ap-south-1",
            "sa-east-1",
        ]

    def create_image_id_list(self):
        region_dictionary = {}
        for region in self.regions:
            ami_id = self._latest_ami_id(region)
            region_dictionary[region] = {"AMI": ami_id}
            print(region)
        return region_dictionary

    def add_mapping_image_id(self):
        self.add_mapping("RegionMap", self.create_image_id_list())

    def add_instance(self):
        disable_ssl_verification_param = self.add_parameter(
            Parameter(
                "disableSslVerification",
                Description="If PRTG doesn't have a valid cert, select true.",
                AllowedValues=["true", "false"],
                Type="String",
            )
        )
        log_file_location_param = self.add_parameter(
            Parameter(
                "logFileLocation",
                Description="Where should the probe log to?",
                Default="/var/log/pyprobe/pyprobe.log",
                Type="String",
            )
        )
        log_level_param = self.add_parameter(
            Parameter(
                "logLevel",
                Description="Where should the probe log to?",
                AllowedValues=["CRITICAL", "ERROR", "WARNING", "INFO", "DEBUG"],
                Default="INFO",
                Type="String",
            )
        )
        probe_access_key_param = self.add_parameter(
            Parameter(
                "probeAccessKey",
                Description="This comes from the PRTG interface.",
                Type="String",
            )
        )
        probe_base_interval_param = self.add_parameter(
            Parameter(
                "probeBaseInterval",
                Description="How often should the probe query the core for new sensor tasks?",
                AllowedValues=["60", "120", "240", "300"],
                Default="60",
                Type="String",
            )
        )
        probe_gid_param = self.add_parameter(
            Parameter(
                "probeGid",
                Description="Enter here a GID for your probe. If you don't want to create one,"
                " the template will for you.",
                Type="String",
            )
        )
        probe_name_param = self.add_parameter(
            Parameter(
                "probeName",
                Description="What should the name of your probe in PRTG be?",
                Default="My Python Probe",
                Type="String",
            )
        )
        prtg_server_ip_dns_param = self.add_parameter(
            Parameter(
                "prtgServerIpDns",
                Description="What's the FQDN of your PRTG instance? i.e. 'prtg.my-prtg.com'",
                Type="String",
            )
        )
        prtg_server_port_param = self.add_parameter(
            Parameter(
                "prtgServerPort",
                Description="Which port does your server listen on for HTTPS? i.e. 443",
                Default="443",
                Type="String",
            )
        )
        probe_task_chunk_size_param = self.add_parameter(
            Parameter(
                "probeTaskChunkSize",
                Description="How many tasks should the probe work on simultaneously?",
                AllowedValues=["20", "50", "100"],
                Default="20",
                Type="String",
            )
        )
        user_data_ps = Join(
            "",
            [
                "#!/bin/bash",
                "\n",
                "sudo apt-get update" + "\n",
                "sudo apt-get install -y python3 python3-dev gcc libyaml-0-2 libyaml-dev libffi-dev"
                " python3-venv python3-pip" + "\n",
                "sudo pip3 install --upgrade setuptools\n" "sudo mkdir /aws-cfn-bootstrap-latest\n",
                "curl https://s3.amazonaws.com/cloudformation-examples/aws-cfn-bootstrap-py3-latest.tar.gz | "
                "tar xz -C /aws-cfn-bootstrap-latest --strip-components 1\n",
                "sudo easy_install /aws-cfn-bootstrap-latest\n",
                "sudo cfn-init -v -r PRTGPyProbeInstance -s",
                RefStackId,
                "--region ",
                RefRegion,
                "\n",
                "sudo python3 -m venv /pyprobe-env" + "\n",
                "source /pyprobe-env/bin/activate" + "\n",
                "sudo /pyprobe-env/bin/python -m pip install --upgrade pip" + "\n",
                "sudo /pyprobe-env/bin/python -m pip install wheel" + "\n",
                "sudo /pyprobe-env/bin/python -m pip install prtg-pyprobe" + "\n",
                "sudo mkdir /etc/pyprobe" + "\n",
                "sudo mkdir /var/log/pyprobe" + "\n",
                "sudo echo disable_ssl_verification: ",
                Ref(disable_ssl_verification_param),
                " > /etc/pyprobe/config.yml\n",
                "sudo echo log_file_location: ",
                Ref(log_file_location_param),
                " >> /etc/pyprobe/config.yml\n",
                "sudo echo probe_access_key: ",
                Ref(probe_access_key_param),
                " >> /etc/pyprobe/config.yml\n",
                'export guid_ext="',
                Ref(probe_gid_param),
                '"\n',
                'if [[ $guid_ext = "" ]]; then echo $(echo "import uuid;print(str(uuid.uuid4()).upper())" | python) '
                "> guid.txt; else echo $guid_ext > guid.txt;fi\n",
                "echo $(echo \"import hashlib; print(hashlib.sha1('",
                Ref(probe_access_key_param),
                "'.encode('utf-8')).hexdigest())\" | python) > hash.txt\n",
                "sudo echo probe_access_key_hashed: $(cat hash.txt) >> /etc/pyprobe/config.yml\n",
                "sudo echo probe_gid: $(cat guid.txt)>> /etc/pyprobe/config.yml\n",
                "sudo echo probe_name: ",
                Ref(probe_name_param),
                " >> /etc/pyprobe/config.yml\n",
                "sudo echo prtg_server_port: ",
                Ref(prtg_server_port_param),
                " >> /etc/pyprobe/config.yml\n",
                "sudo echo prtg_server_ip_dns: ",
                Ref(prtg_server_ip_dns_param),
                " >> /etc/pyprobe/config.yml\n",
                "sudo echo probe_task_chunk_size: ",
                Ref(probe_task_chunk_size_param),
                " >> /etc/pyprobe/config.yml\n",
                "sudo echo probe_base_interval: ",
                Ref(probe_base_interval_param),
                " >> /etc/pyprobe/config.yml\n",
                "sudo echo log_level: ",
                Ref(log_level_param),
                " >> /etc/pyprobe/config.yml\n",
                "sudo /pyprobe-env/bin/pyprobe service install\n",
                "sudo /pyprobe-env/bin/pyprobe service start\n",
                "sudo cfn-signal -e $? --resource PRTGPyProbeInstance --stack ",
                RefStackId,
                " --region ",
                RefRegion,
                "\n",
            ],
        )
        keyname_param = self.add_parameter(
            Parameter(
                "KeyPairName",
                Description="Name of an existing EC2 KeyPair to enable remote access to the instance.",
                Type="AWS::EC2::KeyPair::KeyName",
            )
        )
        vpc_param = self.add_parameter(
            Parameter(
                "VPCId",
                Description="The VPC ID to which the instance will be attached. (Make sure your subnet is in this "
                "VPC and it has internet access)",
                Type="AWS::EC2::VPC::Id",
            )
        )
        subnet_param = self.add_parameter(
            Parameter(
                "SubnetId",
                Description="The ID of the subnet to which this instance should be attached and will monitor."
                " (This must be in the chosen VPC)",
                Type="AWS::EC2::Subnet::Id",
            )
        )
        instance_type_param = self.add_parameter(
            Parameter(
                "InstanceType",
                Type="String",
                AllowedValues=[
                    "t3.nano",
                    "t3.micro",
                    "t3.small",
                    "t3a.nano",
                    "t3a.micro",
                    "t3a.small",
                ],
                Default="t3.small",
                Description="The EC2 instance type.",
            )
        )
        instance_sg = self.add_resource(
            ec2.SecurityGroup(
                "PRTGPyProbeSecurityGroup",
                GroupDescription="No incoming access granted",
                VpcId=Ref(vpc_param),
            )
        )
        instance = ec2.Instance(
            "PRTGPyProbeInstance",
            ImageId=FindInMap("RegionMap", Ref("AWS::Region"), "AMI"),
            CreationPolicy=policies.CreationPolicy(ResourceSignal=policies.ResourceSignal(Count=1, Timeout="PT25M")),
            InstanceType=Ref(instance_type_param),
            KeyName=Ref(keyname_param),
            SubnetId=Ref(subnet_param),
            UserData=Base64(user_data_ps),
            SecurityGroupIds=[GetAtt(instance_sg, "GroupId")],
            Tags=[{"Key": "Name", "Value": "PRTG PyProbe"}],
        )
        self.add_resource(instance)

    @staticmethod
    def _latest_ami_id(region):
        session = boto3.Session(
            aws_access_key_id=os.environ.get("AWS_ACCESS_KEY_ID", None),
            aws_secret_access_key=os.environ.get("AWS_ACCESS_KEY", None),
            region_name=region,
        )
        filters = [
            {"Name": "owner-id", "Values": ["136693071363"]},
            {"Name": "name", "Values": ["debian-10-amd64*"]},
        ]
        result = list(session.resource("ec2").images.filter(Filters=filters))
        latest_ami = result.pop()
        for image in result:
            if latest_ami.name < image.name:
                latest_ami = image
        return latest_ami.id

    @staticmethod
    def render_template():
        stack_template = CloudformationTemplate()
        stack_template.add_instance()
        stack_template.add_mapping_image_id()
        print(stack_template)
        with open("pyprobe_stack_template.json", "w") as template_file:
            template_file.write(json.dumps(stack_template.to_dict(), indent=3))


CloudformationTemplate().render_template()
