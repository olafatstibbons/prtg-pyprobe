# Changelog for pyprobe
## 0.1.0

- Initial release of the pyprobe for PRTG Network Monitor
- Complete overhaul from the old project, to be found at [Github](https://github.com/PRTG/PythonMiniProbe)

## 0.1.1

- Test Coverage for unit test

## 0.1.2

- More test coverage
- Minor changes and fixes

## 0.1.3

- Add correct image tag to readme

## 0.1.4

- Fix Issue #1: Probe crashing with invalid device name
- More tests and coverage
- Minor bugfixes

## 0.3.0
### Improvements
- Task and Data Requests to PRTG Core now async (#26)
- Packages are pinned now (#17)
- String concatenation unified (#15)
- Internal: Rewrite Unit Tests (#2)
- Internal: Refactor main function (#16)
### New Features
- New Sensors: SNMP Traffic (#14), External IP (#8), Temperature (#7), Diskspace (#5), Memory (#4), CPU Load (#3)
### Bugfixes
- Major: pyprobe was not coming up anymore when using 150 Sensors+ (#20)
- Major: Connection Timeout in Port Sensor could crash the probe (#22)
- Minor: various fixes over several issues

## 0.3.1
### Bugfixes
- Major: Add requirement to setup.py

## 0.3.2
### Improvements
- Parallel processed tasks can now be configured.

## 0.5.0
### Improvements
- Select chunk size of parallel processed tasks (#24). If you are upgrading from an older version, you have to edit your `config.yml`. Simply add a line `probe_task_chunk_size: "20"` to the file. You also can adjust the number of parallel processed tasks depending on your hardware. 20 is good for a Rasperry PI 2b running with 1000 sensors. (#24) 
- Documentation: Sensor Help Texts now clearly indicate if a sensor is monitoring the probe machine. (#27)
- Documentation: Now hosted separately at [Gitlab Pages](https://paessler-labs.gitlab.io/prtg-pyprobe/).
### New Features
- Docs are now separately hosted on Gitlab Pages (#19, #29)
- AWS S3 Bucket Sensor (#28)
### Bugfixes
- Major: Wrongly raised exception on async http request backoff caused probe to crash. (#31)
- Minor: Several Bugfixes over different branches.

## 0.5.1
### Improvements
- Documentation examples

## 0.5.2
### Bugfixes
- Major: Ping sensor (TCP mode) could crash the whole probe when target resp port was not available. (#33)

## 0.5.3
### Bugfixes
- Major: Missing requirement caused probe to crash on loading sensors. (#37)

## 1.0.0
### Improvements
- More documentation (#9)
### New Features
- Prometheus Exporter sensor (#34)
- HTTP Rest sensor (#32)
- Home Assistant Plugin (#12)
- Cloudformation Template to make the pyprob able to run in EC2 (#11)
### Bugfixes
- Major: Certificate validation did not work for async http requests (#41)
- Minor: Several small fixes over various branches

## 1.0.1
## Improvements
- Home Assistant Plugin now as git submodule
### Bugfixes
- Fix missing dependency

## 1.0.2
## Improvements
- Some documentation improvements
### Bugfixes
- Fix unit bug with temperatures (#43)

## 1.1.0
## Improvements
- All sensors have now dedicated documentation (#45)
- A flow chart on how the probe works is available (#18)
## New Feature
- Script Sensor (#47)


## 1.1.1
## Bugfix
- Formatting of Helm Chart (!61)

